import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react'
import { logOut } from '../slices/authSlice'
import { setCredentials } from '../slices/authSlice'

const baseQuery = fetchBaseQuery({
    baseUrl: 'http://localhost:8000/api',
    credentials: 'include',
    prepareHeaders: (headers, {getState})=> {
        const access_token = getState().auth.access_token
        if (access_token) {
            headers.set("Authorization", `Bearer ${access_token}`)
        }
        return headers
    }
})

const baseQueryWithReauth = async (args, api, extraOptions) => {
    let result = await baseQuery(args, api, extraOptions)

    if (result?.error?.originalStatus === 403) {
        console.log('trying refresh')

        const refreshResult = await baseQuery('/auth/refresh', api, extraOptions)

        console.log(refreshResult)
        if (refreshResult?.data)  {

            api.dispatch(setCredentials({...refreshResult.data}))
            result = await baseQuery(args, api, extraOptions)

        } else {
            api.dispatch(logOut())
        }
    }

    return result
}

export const authQueryApi = createApi({
    reducerPath: 'authQuery',
    baseQuery: baseQueryWithReauth,
    endpoints: builder => ({}),

})
