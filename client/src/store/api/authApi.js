import { authQueryApi } from './authQueryApi'


const authTags = authQueryApi.enhanceEndpoints({addTagTypes:['Token', 'todolists', 'todos'],})

export const authApi = authQueryApi.injectEndpoints({
    endpoints: builder => ({
        login: builder.mutation({
            query: data => ({
                url: '/auth/token',
                method: 'POST',
                body: data,
                credentials: 'include',
            })
        }),
        signUp: builder.mutation({
            query: data => ({
                url:'/auth/signup',
                method: 'POST',
                body: data
            })
        }),
        createTodoList: builder.mutation({
            query: content => ({
                url:'/todolists',
                method: 'POST',
                body: content,
                credientials: 'include'
            }),
            invalidatesTags: ['todolists']
        }),
        editTodoList: builder.mutation({
            query: data => ({
                    url:`/todolists/${data.todolist_id}`,
                    method: 'PUT',
                    body: data.content,
                    credientials: 'include'
            }),
            invalidatesTags: ['todolists']
        }),
        deleteTodoList: builder.mutation({
            query: todo_id => ({
                url:`/todolists/${todo_id}`,
                method: 'DELETE',
                credientials: 'include'
            }),
            invalidatesTags: ['todolists']
        }),
        createTodo: builder.mutation({
            query: data => ({
                url:`/todolists/${data.todolist_id}/todos`,
                method: 'POST',
                body: data.content,
                credientials: 'include'
            }),
            invalidatesTags: ['todos']
        }),
        editTodo: builder.mutation({
            query: content => {
                console.log("QUERY CONTENT",content)
                return ({
                    url:`/todolists/${content.todolist_id}/todos/${content.id}`,
                    method: 'PUT',
                    body: content,
                    credientials: 'include'
                })
            },
            invalidatesTags: ['todos']
        }),
        deleteTodo: builder.mutation({
            query: data => ({
                url:`/todolists/${data.list_id}/todos/${data.todo_id}`,
                method: 'DELETE',
                credientials: 'include'
            }),
            invalidatesTags: ['todos']
        }),
        getAllTodoLists: builder.query({
            query: () => ({
                url: `/todolists`,
                method: 'GET',
                credientials: 'include'
            }),
            providesTags: ['todolists']
        }),
        getOneTodoList: builder.query({
            query: id => ({
                url: `/todolists/${id}`,
                method: 'GET',
                credientials: 'include'
            }),
            providesTags: ['todolists']
        }),
        getAllTodos: builder.query({
            query: list_id => ({
                url: `/todolists/${list_id}/todos`,
                method: 'GET',
                credientials: 'include'
            }),
            providesTags: ['todos']
        }),
    })

})

export const {
    useLoginMutation,
    useSignUpMutation,
    useCreateTodoListMutation,
    useEditTodoListMutation,
    useDeleteTodoListMutation,
    useCreateTodoMutation,
    useEditTodoMutation,
    useDeleteTodoMutation,
    useGetAllTodoListsQuery,
    useGetOneTodoListQuery,
    useGetAllTodosQuery,
    useGetOneTodoQuery
} = authApi
