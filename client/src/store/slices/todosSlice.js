import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    todos:[],
    list_id: null,
    editTodo: {}
}

const todoSlice = createSlice({
    name:'todos',
    initialState,
    reducers: {
        setTodos: (state, {payload}) => {
            state.todos = [...payload]
        },
        setEditTodo: (state, {payload}) => {
            const formatedDate = new Date(payload.due_date).toISOString().slice(0, 10);
            const newTodo = {...payload, "due_date": formatedDate}
            state.editTodo = newTodo
        },
        setListId: (state, {payload}) => {
            state.list_id = payload
        },
        resetTodos: (state, ) => {
            state.todos = []
            state.list_id = null
            state.editTodo = {}
        }

    }
})

export const {setTodos, setEditTodo, setListId, resetTodos} = todoSlice.actions
export default todoSlice.reducer

export const selectTodos = (state) => state.todos
