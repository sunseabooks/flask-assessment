import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    todolists:[],
    editList_id:null
}

const todoListsSlice = createSlice({
    name:'todoLists',
    initialState,
    reducers: {
        setTodoLists: (state, {payload}) => {
            state.todolists = [...payload]
        },
        resetLists: (state) => {
            state.todolists = []
        },
        setEditList: (state, {payload}) => {
            state.editList_id = payload
        }
    }
})

export const {setTodoLists, resetLists, setEditList} = todoListsSlice.actions
export default todoListsSlice.reducer

export const selectTodoLists = (state) => state.todoLists
