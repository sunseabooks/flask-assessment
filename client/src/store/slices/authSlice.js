import {createSlice} from  '@reduxjs/toolkit'

const initialState = {
    user: null,
    access_token: null,
    refresh_token: null
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setCredentials: (state, {payload}) => {
            const { user, access_token, refresh_token } = payload
            state.user = user
            state.access_token = access_token
            state.refresh_token = refresh_token
        },
        logOut: (state) => {
            state.user = null
            state.access_token = null
            state.refresh_token = null
        }
    }
})

export const {setCredentials, logOut} = authSlice.actions

export default authSlice.reducer
export const currUser = (state) => state.auth.user
export const currToken = (state) => state.auth.access_token
export const currRefresh = (state) => state.auth.refresh_token
