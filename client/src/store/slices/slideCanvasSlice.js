import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    isOpen: false,
    content: ""
}

const slideCanvasSlice = createSlice({
    name:'slideCanvas',
    initialState,
    reducers: {
        setCanvas: (state, {payload}) => {
            state.isOpen = payload.isOpen
            state.content = payload?.content || state.content
        },
        resetCanvas: (state) => {
            state.isOpen = ""
            state.content = ""
        }
    }

})

export const {setCanvas, resetCanvas} = slideCanvasSlice.actions
export default slideCanvasSlice.reducer

export const selectSlideCanvas = (state) => state.slideCanvas
