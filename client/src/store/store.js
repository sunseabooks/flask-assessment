import {configureStore} from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/dist/query'
import { authQueryApi } from './api/authQueryApi'
import authSliceReducer from './slices/authSlice'
import slideCanvasReducer from './slices/slideCanvasSlice'
import todoListsReducer from './slices/todoListsSlice'
import todosReducer from './slices/todosSlice'

export const store = configureStore({
    reducer: {
        [authQueryApi.reducerPath]: authQueryApi.reducer,
        auth: authSliceReducer,
        slideCanvas: slideCanvasReducer,
        todoLists: todoListsReducer,
        todos: todosReducer,
    },
    middleware: getDefaultMiddleware =>
        getDefaultMiddleware().concat(authQueryApi.middleware),
    devTools: true
})

setupListeners(store.dispatch)
