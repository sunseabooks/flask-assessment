import {Container} from 'react-bootstrap'
import {Routes, Route} from 'react-router-dom'
import HomePage from './pages/HomePage'
import TodosPage from './pages/TodosPage'
import AboutPage from './pages/AboutPage'
import TodoListsPage from './pages/TodoListsPage'
import NavBarComponent from './components/NavBarComponent'
import SlideCanvas from './components/SlideCanvas'

function App() {

  return (
    <>
    <NavBarComponent />
    <Container>
        <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/about" element={<AboutPage />} />
            <Route path="/lists" >
                <Route index element={<TodoListsPage /> } />
                <Route path=":list_id">
                    <Route index element={<TodosPage />} />
                </Route>
            </Route>
        </Routes>
    </Container>
    <main>

    <SlideCanvas className="h-auto" />
    </main>
    </>
  )
}

export default App
