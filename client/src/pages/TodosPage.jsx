import React, {useEffect, useState} from 'react'
import {Container, Col, Row, Button, Accordion} from 'react-bootstrap'
import { useGetAllTodosQuery, useDeleteTodoMutation, useEditTodoMutation } from '../store/api/authApi'
import {useParams} from 'react-router-dom'
import {useDispatch, useSelector} from 'react-redux'
import {selectTodos, setTodos, setListId, setEditTodo} from '../store/slices/todosSlice'
import { faCalendarPlus, faTrashCan, faCalendarCheck } from '@fortawesome/free-regular-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {setCanvas} from "../store/slices/slideCanvasSlice"
import moment from 'moment'

const showCreateTodo = {
    isOpen:true,
    content: "createTodo"
}
const showEditTodo = {
    isOpen:true,
    content: "editTodo"
}
export default function TodosPage() {
    const [highlight, setHighlight] = useState(false)
    const {todos} = useSelector(selectTodos)
    const dispatch = useDispatch()
    const {list_id} = useParams()
    const [deleteTodo, deleteResult] = useDeleteTodoMutation()
    const [updateTodo, editResult] = useEditTodoMutation()
    const {data, isLoading} = useGetAllTodosQuery(list_id)

    useEffect(()=>{
        if (data) {
            dispatch(setTodos(data.todos))
            dispatch(setListId(list_id))
        }
    }, [data])

    function onDragHandler(e, todo_id) {
        e.dataTransfer.setData("todo_id", todo_id )
    }
    async function onDropHandler(e) {
        const todo_id = e.dataTransfer.getData("todo_id")
        setHighlight(false)
        await deleteTodo({todo_id, list_id}).unwrap()
        .then((res) => {
            console.log(res)
        }).catch((err)=>console.log(err))
    }
    function onDragOverHandler(e) {
        e.preventDefault()
        setHighlight(true)
    }
    async function editHandler(e) {
        const id = e.target.value
        const target = e.target.id
        const todo = todos.filter(todo=>todo.id==id)[0]

        if (target === "editButton") {
            dispatch(setEditTodo(todo))
            dispatch(setCanvas(showEditTodo))
        }
        if (target === "doneButton") {
            const newTodo = {...todo, done:!todo["done"]}
            await updateTodo(newTodo).unwrap()
            .then(res=>console.log(res))
            .catch(err=>console.log(err))
        }
    }
    function datehandler(todo) {
        const curr = moment()
        const due_date = moment(todo.due_date)
        const date_diff = curr.diff(due_date, 'days')

        return todo.done? "text-success":
        date_diff>0? "text-danger" :
        date_diff<0? "text-primary" : "text-warning"
    }
  return (
    <>
    <Container className="w-75 mt-5">
        <Container className="d-flex justify-content-around md mb-4">
            <Container className="d-flex justify-content-start align-items-end me-auto gap-3">
                <Button className="ms-2" onClick={()=>dispatch(setCanvas(showCreateTodo))}>
                    <FontAwesomeIcon icon={faCalendarPlus} size="2xl" />
                </Button>
                <h1 style={{marginBottom:"0"}}> list {list_id} </h1>
            </Container>

                <Container className="d-flex justify-content-end w-50" >
                    <FontAwesomeIcon className={highlight? "text-danger": ""}
                    icon={faTrashCan} size="4x" onDrop={onDropHandler} onDragOver={onDragOverHandler}
                    onDragEnter={()=>setHighlight(true)} onDragLeave={()=>setHighlight(false)}/>
                </Container>

        </Container>
        <Col>

            <Accordion defaultActiveKey="0" flush>
            {
                todos?
                    todos.map((todo, index)=>
                    <Accordion.Item
                    draggable onDragStart={(e)=> onDragHandler(e, todo.id)}
                    key={todo.id} eventKey={index}>
                        <Accordion.Header >

                            <Container>
                                <p>Order {todo.order}</p>
                            </Container>
                            <Container  >
                                <h5>{todo.title}</h5>
                            </Container>
                            <Container>
                                <p>{moment(todo.due_date).add(1, 'day').format("dddd, MMMM Do YYYY")}</p>
                            </Container>
                            <Container className="d-flex justify-content-end w-50" >
                                <FontAwesomeIcon
                                className={datehandler(todo)} icon={faCalendarCheck} size="2xl" />
                            </Container>

                        </Accordion.Header>

                        <Accordion.Body>
                        <Container className="d-flex flex-col">
                            <Container className="w-75">
                            <p>
                            {todo.description}

                            </p>
                            </Container>
                            <Container className="d-flex flex-row w-25 gap-1
                            justify-content-end align-items-end">
                                <Button id="editButton" value={todo.id} onClick={editHandler} >Edit</Button>
                                <Button id="doneButton" value={todo.id} onClick={editHandler}>Done</Button>
                            </Container>

                        </Container>

                        </Accordion.Body>
                    </Accordion.Item>

                    )
                :<Accordion.Header>Nothing to do...</Accordion.Header>
            }

            </Accordion>

        </Col>

    </Container>

    </>
  )
}
