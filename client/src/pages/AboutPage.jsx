import React from 'react'
import {Container} from 'react-bootstrap'
import Image from 'react-bootstrap/Image'
import Sophia_Hu_Resume from '../assets/Sophia_hu_Resume.png'
import TwoFish_wireframe from '../assets/TwoFish_wireframe.svg'
import profile_pic from '../assets/profile_pic.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faGitlab, faLinkedinIn} from '@fortawesome/free-brands-svg-icons'

export default function AboutPage() {
  return (
    <>
        <Container className="w-75 mt-5">
            <Container className="d-flex flex-column align-items-start justify-content-center
            bg-white shadow-sm mt-5 gap-3 p-5 border border-2 rounded-3">
                    <Container className="d-flex flex-row shadow-sm border border-2 rounded-3">
                    <Container className="d-flex flex-column gap-2 ">
                        <h1 >Sophia Hu</h1>
                        <Container className="d-flex flex-row gap-3">
                            <a href="https://gitlab.com/sunseabooks" target="_blank" rel="noopener noreferrer">
                                <FontAwesomeIcon icon={faGitlab} style={{color: "#e3842b",}} size="2xl"/>
                            </a>
                            <a href="https://www.linkedin.com/in/super-sophia-hu/" target="_blank" rel="noopener noreferrer">
                                <FontAwesomeIcon icon={faLinkedinIn} style={{color: "#2460c6",}} size="2xl"/>
                            </a>
                        </Container>
                        <p>Full Stack Engineer | Python | JavaScript | React.js | Redux.js | PostgreSQL | Django Framework | FastAPI | RESTful Architecture | Architect of Thought |</p>
                    </Container>
                        <Container className="d-flex justify-content-center align-content-center py-2">
                            <Image className="w-100 h-95" src={profile_pic} alt="profile pic" roundedCircle style={{objectFit:"cover", maxWidth:"250px"}} />
                        </Container>
                    </Container>

                    <p>Here is my wireframe for the project.</p>
                    <p>I'm proud that I was able to make most of them happen, but definitely some fell to the cutting room floor</p>

                    <Image src={TwoFish_wireframe} alt="wireframe" style={{objectFit:"cover", maxWidth:"100%"}} />
                    <p>hire me!</p>
                    <Image src={Sophia_Hu_Resume} alt="my resume" style={{objectFit:"cover", maxWidth:"100%"}} />

            </Container>
        </Container>
    </>
  )
}
