import React from 'react'
import {useSelector} from 'react-redux'
import {currUser} from '../store/slices/authSlice'
import {Container, Image} from 'react-bootstrap'
import calendar_system from '../assets/calendar_system.svg'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faTrashCan } from '@fortawesome/free-regular-svg-icons'

export default function HomePage() {
    const user = useSelector(currUser)

  return (
    <>
    <Container className="w-75 mt-5">
        <Container className="d-flex flex-column align-items-center justify-content-center
        bg-white shadow-sm mt-5 py-3 gap-2 border border-2 rounded-3">
        <h1>Welcome to my app!</h1>
        {user ?
            <>
                <div>Enjoy your stay {user.name}!</div>
                <p>To delete you can drag&drop any list/todo to
                <FontAwesomeIcon className="ms-1"icon={faTrashCan} size="lg" />
                </p>
                <p>Here's the color code of the calender system!</p>
                <Container>
                    <Image src={calendar_system} style={{objectFit:"cover", marge:"0", width:"100%"}} />
                </Container>
            </>
            :<>
                <Container className="d-flex flex-column
                bg-white shadow-sm mt-5 gap-2">
                <p className="mt-2"> Hi Will, Monserrat, and whomever else it may concern.</p>
                <p>Welcome to my site!</p>

                <p>
                    Feel free to sign up to start testing out the features.
                    This was a really fun project,
                    I enjoyed learning flask and hope to do more of it in the future!
                </p>
                </Container>
            </>
        }
        </Container>
    </Container>

    </>
  )
}
