import React, {useEffect, useState} from 'react'
import {Container} from 'react-bootstrap'
import TodoListsCard from '../components/cards/TodoListCard'
import {useGetAllTodoListsQuery, useDeleteTodoListMutation} from '../store/api/authApi/'
import {useSelector, useDispatch} from 'react-redux'
import {faTrashCan } from '@fortawesome/free-regular-svg-icons'
import {faBed} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { selectTodoLists, setTodoLists } from '../store/slices/todoListsSlice'

export default function TodoListsPage() {
    const [highlight, setHighlight] = useState(false)
    const {data} = useGetAllTodoListsQuery()
    const {todolists} = useSelector(selectTodoLists)
    const [deleteTodoList, result] = useDeleteTodoListMutation()
    const dispatch = useDispatch()

    useEffect(()=>{
        if (data) {
            dispatch(setTodoLists(data.todolists))
        }
    },[data])

    function onDragHandler(e, list_id) {
        e.dataTransfer.setData("list_id", list_id )
    }
    async function onDropHandler(e) {
        const list_id = e.dataTransfer.getData("list_id")
        console.log("list_id", list_id)
        setHighlight(false)
        await deleteTodoList(list_id).unwrap()
        .then((res) => {
            console.log(res)
        }).catch((err)=>console.log(err))
    }
    function onDragOverHandler(e) {
        e.preventDefault()
        setHighlight(true)
    }

  return (
    <>
        <Container className="w-75 mt-5">
            <Container className="d-flex justify-content-around md mb-4">

                <Container className="d-flex justify-content-start align-items-end me-auto gap-3">
                    <h1 style={{marginBottom:"0"}}> My lists</h1>
                </Container>

                    <Container className="d-flex justify-content-end w-50">

                        <FontAwesomeIcon className={highlight? "text-danger": ""}
                        icon={faTrashCan} size="4x" onDrop={onDropHandler} onDragOver={onDragOverHandler}
                        onDragEnter={()=>setHighlight(true)} onDragLeave={()=>setHighlight(false)}/>
                    </Container>

            </Container>
            <Container className="d-flex flex-column
            justify-content-around align-items-center pt-3 pb-3 mt-5 gap-1">

                {todolists.length>0? todolists.map((todolist, index)=>
                    <Container className="w-100" key={index} draggable onDragStart={(e)=> onDragHandler(e, todolist.id)} >
                        <TodoListsCard list={...todolist} />
                    </Container>
                )
                :
                <>
                    <span >Nothing to do ...  </span>
                    <FontAwesomeIcon icon={faBed} size="4x" />
                </>
                }
            </Container>

        </Container>

    </>
  )
}
