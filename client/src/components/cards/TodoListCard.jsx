import React from 'react'
import {Card, Button, Container} from 'react-bootstrap'
import {useNavigate} from 'react-router-dom'
import {useDispatch} from 'react-redux'
import { setCanvas } from '../../store/slices/slideCanvasSlice'
import { setEditList } from '../../store/slices/todoListsSlice'

const showEditTodoList = {
    isOpen:true,
    content: "editTodoList"
}

export default function TodoListCard({list:{id, title, created_on}}) {
    const navigate = useNavigate()
    const dispatch = useDispatch()
    function editHandler(e) {
        dispatch(setEditList(e.target.value))
        dispatch(setCanvas(showEditTodoList))
    }

  return (
    <Card className="border border-0  shadow-sm bg-white" >
        <Card.Body className="d-flex flex-column ">
            <Card.Title className = "d-flex flex-row justify-content-around align-items-baseline">
                <Container className="d-flex flex-row justify-content-start gap-3 me-auto w-75" onClick={()=>navigate(`/lists/${id}`)}>
                    <span className="fs-2 me-auto">{title}</span>
                    <p>Created on {created_on}</p>
                </Container>
                <Button id="editButton" value={id} onClick={editHandler} >Edit</Button>

            </Card.Title>
        </Card.Body>
    </Card>
  )
}
