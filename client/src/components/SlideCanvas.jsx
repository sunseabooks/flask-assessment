import React, {useState} from 'react'
import {Offcanvas, Container, CloseButton} from 'react-bootstrap'
import LoginForm from './forms/LoginForm'
import CreateTodoForm from './forms/CreateTodoForm'
import EditTodoForm from './forms/EditTodoForm'
import CreateTodoListForm from './forms/CreateTodoListForm'
import SignUpForm from './forms/SignUpForm'
import {selectSlideCanvas} from '../store/slices/slideCanvasSlice'
import {useSelector, useDispatch} from 'react-redux'
import {setCanvas} from '../store/slices/slideCanvasSlice'
import EditTodoListForm from './forms/EditListForm'


const contentRef = {
    login: <LoginForm />,
    signup: <SignUpForm />,
    createTodoList: <CreateTodoListForm />,
    editTodo: <EditTodoForm />,
    createTodo: <CreateTodoForm />,
    editTodoList: <EditTodoListForm />
}
const closeCanvas = {isOpen: false}

export default function SlideCanvas() {
    const {isOpen, content} = useSelector(selectSlideCanvas)
    const dispatch = useDispatch()
    console.log(isOpen)
  return (
    <Offcanvas onHide={()=>dispatch(setCanvas(closeCanvas))} show={isOpen} placement="end" scroll={true}>
        <Container className="d-flex flex-column justify-content-center align-content-center mt-5 h-35">

            <Offcanvas.Header>
                <CloseButton onClick={()=>dispatch(setCanvas(closeCanvas))}/>
            </Offcanvas.Header>

            <Offcanvas.Title className="mt-3">
                {contentRef[content]}
            </Offcanvas.Title>

        </Container>
    </Offcanvas>
  )
}
