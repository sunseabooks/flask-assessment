import React, {useState} from 'react'
import {Form, Button, Container} from 'react-bootstrap'
import { useEditTodoListMutation } from '../../store/api/authApi'
import {useDispatch, useSelector} from 'react-redux'
import {setTodoLists} from '../../store/slices/todoListsSlice'
import {resetCanvas} from '../../store/slices/slideCanvasSlice'
import {selectTodoLists} from '../../store/slices/todoListsSlice'


export default function EditTodoListForm() {
    const [updateList, result] = useEditTodoListMutation()
    const [title, setTitle] = useState("")
    const dispatch = useDispatch()
    const {editList_id} = useSelector(selectTodoLists)

    async function handleSubmit(e) {
        e.preventDefault()
        const data = {
            todolist_id:editList_id,
            content:{"title":title}
        }
        await updateList(data).unwrap()
        .then((res)=>{
            dispatch(setTodoLists([res]))
            console.log(res)
            dispatch(resetCanvas())
        }).catch((err)=>console.log(err))
    }
  return (
    <>
    <Container className="d-flex flex-column justify-content-center align-content-center">

        <h1>Update your List!</h1>
        <Form onSubmit={handleSubmit}>
            <Container className="d-flex flex-column gap-3">
                <Form.Group>
                    <Form.Label>Create a new title</Form.Label>
                    <Form.Control value={title} onChange={(e) =>setTitle(e.target.value)} type="text" placeholder="title of your new list" />
                </Form.Group>
                <Button type="submit" >Update List</Button>

            </Container>
        </Form>
    </Container>
    </>
  )
}
