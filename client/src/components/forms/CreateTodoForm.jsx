import React, {useState} from 'react'
import {Form, Button, Container} from 'react-bootstrap'
import { useCreateTodoMutation, useEditTodoMutation } from '../../store/api/authApi'
import {useDispatch} from 'react-redux'
import {resetCanvas} from '../../store/slices/slideCanvasSlice'
import { useSelector } from 'react-redux'
import { selectTodos } from '../../store/slices/todosSlice'


const initialState = {
    "title": "",
    "description": "",
    "due_date": "",
    "done": false,
    "order": 1,
}

export default function CreateTodoListForm() {
    const [createTodo, result] = useCreateTodoMutation()
    const [updateTodo, editResult] = useEditTodoMutation()
    const [todo, setTodo] = useState(initialState)
    const dispatch = useDispatch()
    const {todos, list_id} = useSelector(selectTodos)

    function updateOrder(newTodo) {
        try {
            const copy = [...todos]
            const infront = copy.filter(todo=>todo.order<newTodo.order)
                            .sort((curr,next)=> {curr.order-next.order})

            const behind = copy.filter(todo=>todo.order>=newTodo.order)
                            .sort((curr,next)=> {curr.order-next.order})

            const tmp = [...infront, newTodo, ...behind]
            return tmp.map((todo, index)=> ({...todo, order:index+1}))
        } catch (error) {
            console.error(error)
        }
    }

    async function handleSubmit(e, todoData) {
        e.preventDefault()

        const todolist_id = Number(list_id)
        const content = {...todoData, todolist_id, order:Number(todoData.order)}

        await createTodo({content, todolist_id}).unwrap()
        .then( async (res)=>{
            const orderedTodos = updateOrder(res)
            const promises = orderedTodos.map(todo=>{
                return updateTodo(todo)
            })
            await Promise.all(promises)
            .then(()=>dispatch(resetCanvas()))
            .catch(err=>console.log(err))

        }).catch((err)=>console.log(err))
    }

    function inputHandler(e) {
        let property = e.target.id
        let value = e.target.value
        setTodo({...todo, [property]:value})
    }

  return (
    <>
    <h1>Create a New todo!</h1>
    <Form onSubmit={(e)=>handleSubmit(e, todo)}>
    <Form.Group>
        <Form.Label>Title of your new todo</Form.Label>
        <Form.Control id="title" value={todo.title} onChange={inputHandler} type="text" placeholder="" />
    </Form.Group>

    <Form.Group>
        <Form.Label>Description</Form.Label>
        <Form.Control id="description" value={todo.description} onChange={inputHandler} as="textarea" placeholder="" />
    </Form.Group>

    <Form.Group>
        <Form.Label>Due date</Form.Label>
        <Form.Control id="due_date" value={todo.due_date} onChange={inputHandler} type="date" placeholder="" />
    </Form.Group>


    <Form.Group>
        <Form.Label>Current Order {todo.order}</Form.Label>
        <Form.Control id="order" value={todo.order} onChange={inputHandler} type="range" min="1" max={todos.length+1} placeholder="" />
        <Container className="d-flex">
            <Form.Label className="me-auto">Min 1</Form.Label>
            <Form.Label>Max {todos.length +1}</Form.Label>
        </Container>
    </Form.Group>

    <Button type="submit" >Create Todo</Button>
    </Form>
    </>
  )
}
