import React, {useState} from 'react'
import {Form, Button, Container} from 'react-bootstrap'
import { useEditTodoMutation } from '../../store/api/authApi'
import {useDispatch} from 'react-redux'
import {resetCanvas} from '../../store/slices/slideCanvasSlice'
import {setTodos} from '../../store/slices/todosSlice'
import { useSelector } from 'react-redux'
import { selectTodos } from '../../store/slices/todosSlice'

export default function EditTodoForm() {
    const {editTodo, todos} = useSelector(selectTodos)
    const [todo, setTodo] = useState({...editTodo})
    const [updateTodo, result] = useEditTodoMutation()
    const dispatch = useDispatch()

    function updateOrder(newTodo) {
        try {
            const copy = [...todos].filter(todo=>todo.id!==newTodo.id)

            const infront = copy.filter(todo=>todo.order<=newTodo.order)
                            .sort((curr,next)=> {curr.order-next.order})

            const behind = copy.filter(todo=>todo.order>newTodo.order)
                            .sort((curr,next)=> {curr.order-next.order})

            const tmp = newTodo.order<2 ? [newTodo, ...copy] : [...infront, newTodo, ...behind]

            return tmp.map((todo, index)=> ({...todo, order:index+1}))
        } catch (error) {
            console.error(error)
        }
    }

    async function handleSubmit(e, editedTodo) {
        e.preventDefault()
        const content = {...editedTodo, order:Number(editedTodo.order)}
        await updateTodo(content).unwrap()
        .then( async (res)=>{
            const orderedTodos = updateOrder(res.todo)
            const promises = orderedTodos.map(todo=>{
                return updateTodo(todo)
            })
            await Promise.all(promises)
            .then(()=>dispatch(resetCanvas()))
            .catch(err=>console.log(err))

        }).catch((err)=>console.log(err))
    }

    function inputHandler(e) {
        let property = e.target.id
        let value = e.target.value
        setTodo({...todo, [property]:value})
    }


  return (
    <>
    <h1>Update your todo!</h1>
    <Form onSubmit={(e)=>handleSubmit(e, todo)}>
    <Form.Group>
        <Form.Label>Title of your todo</Form.Label>
        <Form.Control id="title" value={todo.title} onChange={inputHandler} type="text" placeholder="" />
    </Form.Group>

    <Form.Group>
        <Form.Label>Description</Form.Label>
        <Form.Control id="description" value={todo.description} onChange={inputHandler} as="textarea" placeholder="" />
    </Form.Group>

    <Form.Group>
        <Form.Label>Due date</Form.Label>
        <Form.Control id="due_date" value={todo.due_date} onChange={inputHandler} type="date" placeholder="" />
    </Form.Group>


    <Form.Group>
        <Form.Label>Current Order {todo.order}</Form.Label>
        <Form.Control id="order" value={todo.order} onChange={inputHandler} type="range" min="1" max={todos.length} placeholder="" />
        <Container className="d-flex">
            <Form.Label className="me-auto">Min 1</Form.Label>
            <Form.Label>Max {todos.length}</Form.Label>
        </Container>
    </Form.Group>

    <Button type="submit" >Update!</Button>
    </Form>
    </>
  )
}
