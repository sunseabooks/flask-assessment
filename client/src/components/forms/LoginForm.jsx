import React, {useState} from 'react'
import { useNavigate} from 'react-router-dom'
import {Form, Button, Container} from 'react-bootstrap'
import {useDispatch} from 'react-redux'
import {setCredentials} from '../../store/slices/authSlice'
import { useLoginMutation } from '../../store/api/authApi'
import {resetCanvas} from '../../store/slices/slideCanvasSlice'

const loginDataInit = {
    username: "",
    password: ""
}

export default function LoginForm() {
    const [loginData, setLoginData] = useState(loginDataInit)
    const navigate = useNavigate()

    const [login, result] = useLoginMutation()
    const dispatch = useDispatch()

    function inputHandler(e) {
        setLoginData({...loginData, [e.target.id]:e.target.value})
    }

    async function handleSubmit(e) {
        e.preventDefault()
        const {username, password} = loginData

        await login({username, password}).unwrap()
        .then((res)=>{
            dispatch(setCredentials({...res}))
            console.log(res)

            dispatch(resetCanvas())
        }).catch(console.log(result))

        setLoginData(loginDataInit)
        navigate('/')

    }
  return (
    <>
        <Form onSubmit={handleSubmit}>
        <Container className="d-flex flex-column gap-3">
            <Form.Group>
                <Form.Label>Username</Form.Label>
                <Form.Control onChange={inputHandler} value={loginData.username}
                id="username" type="text" placeholder="username" />
            </Form.Group>

            <Form.Group>
                <Form.Label>Password</Form.Label>
                <Form.Control onChange={inputHandler} value={loginData.password}
                id="password" type="password" placeholder="password" />
            </Form.Group>
            <Button type="submit">
                Login
            </Button>

        </Container>
        </Form>
    </>
  )
}
