import React, {useState} from 'react'
import {Form, Button, Container} from 'react-bootstrap'
import { useCreateTodoListMutation } from '../../store/api/authApi'
import {useDispatch} from 'react-redux'
import {setTodoLists} from '../../store/slices/todoListsSlice'
import {resetCanvas} from '../../store/slices/slideCanvasSlice'


export default function CreateTodoListForm() {
    const [createList, result] = useCreateTodoListMutation()
    const [title, setTitle] = useState("")
    const dispatch = useDispatch()

    async function handleSubmit(e) {
        e.preventDefault()
        await createList({title}).unwrap()
        .then((res)=>{
            dispatch(setTodoLists([res]))
            dispatch(resetCanvas())
        }).catch((err)=>console.log(err))
    }
  return (
    <>
    <Container className="d-flex flex-column justify-content-center align-content-center">

        <h1>Create a New List!</h1>
        <Form onSubmit={handleSubmit}>
            <Container className="d-flex flex-column gap-3">
                <Form.Group>
                    <Form.Label>Title of your new list</Form.Label>
                    <Form.Control value={title} onChange={(e) =>setTitle(e.target.value)} type="text" placeholder="title of your new list" />
                </Form.Group>
                <Button type="submit" >Create List</Button>

            </Container>
        </Form>
    </Container>
    </>
  )
}
