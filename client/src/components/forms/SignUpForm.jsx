import React, {useState} from 'react'
import {useDispatch} from 'react-redux'
import {Container, Form, Button} from 'react-bootstrap'
import {useSignUpMutation} from '../../store/api/authApi'
import {resetCanvas} from '../../store/slices/slideCanvasSlice'
import {setCredentials} from '../../store/slices/authSlice'


const initialState = {
    "username": "",
    "password": "",
    "name": "",
    "email": ""
}

export default function SignUpForm() {
    const [signUp, result] = useSignUpMutation()
    const [formData, setFormData] = useState(initialState)
    const dispatch = useDispatch()


    function inputHandler(e) {
        let property = e.target.id
        let value = e.target.value
        setFormData({...formData, [property]:value})
    }

    async function submitHandler(e) {
        e.preventDefault()
        await signUp(formData).unwrap()
        .then((res)=>{
            dispatch(setCredentials({...res}))
            setFormData(initialState)
            dispatch(resetCanvas())
        }).catch(console.log(result))
    }
  return (

    <>
        <Form onSubmit={submitHandler}>
        <Container className="d-flex flex-column gap-3">

            <Form.Group>
                <Form.Label>Username</Form.Label>
                <Form.Control onChange={inputHandler} value={formData.username}
                id="username" type="text" placeholder="username" />
            </Form.Group>

            <Form.Group>
                <Form.Label>name</Form.Label>
                <Form.Control onChange={inputHandler} value={formData.name}
                id="name" type="text" placeholder="name" />
            </Form.Group>

            <Form.Group>
                <Form.Label>Password</Form.Label>
                <Form.Control onChange={inputHandler} value={formData.password}
                id="password" type="password" placeholder="password" />
            </Form.Group>

            <Form.Group>
                <Form.Label>email</Form.Label>
                <Form.Control onChange={inputHandler} value={formData.email}
                id="email" type="email" placeholder="email" />
            </Form.Group>

            <Button type="submit">
                Create Account!
            </Button>

        </Container>

        </Form>
    </>

  )
}
