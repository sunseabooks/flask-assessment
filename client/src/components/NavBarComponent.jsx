import React, {useState} from 'react'
import { Container, Nav, Navbar, Button} from 'react-bootstrap'
import {NavLink, Link, useNavigate} from 'react-router-dom'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {authApi} from '../store/api/authApi'
import {faFish,faListCheck} from '@fortawesome/free-solid-svg-icons'
import {useSelector, useDispatch} from 'react-redux'
import {currToken, logOut} from '../store/slices/authSlice'
import {setCanvas} from '../store/slices/slideCanvasSlice'
import {resetTodos} from '../store/slices/todosSlice'
import {resetLists} from '../store/slices/todoListsSlice'

const showLogin = {isOpen:true, content:"login"}
const showSignUp = {isOpen:true, content:"signup"}
const showCreateTodoList = {isOpen:true, content:"createTodoList"}

export default function NavBarComponent() {
    const token = useSelector(currToken)
    const dispatch = useDispatch()
    const navigate = useNavigate()

    function logOutHandler() {
        dispatch(logOut())
        dispatch(resetLists())
        dispatch(resetTodos())
        dispatch(authApi.util.resetApiState())
        navigate('/')
    }

  return (
    <Navbar className="d-flex justify-content-evenly
    bg-white shadow-sm mb-5 py-4">
        <Container>
            <Container>
                <Nav className="me-auto">
                    <Nav.Link to="/" as={NavLink}>
                        Home
                    </Nav.Link>
                    <Nav.Link to="/about" as={NavLink}>
                        About
                    </Nav.Link>
                    <Nav.Link to="/lists" as={NavLink}>
                        Lists
                    </Nav.Link>
                </Nav>

            </Container>

            <Container className="d-flex justify-content-center">
                <Link to="/" as={NavLink}>
                <span>
                    <FontAwesomeIcon icon={faFish} rotation={180} size="2xl" />
                    <FontAwesomeIcon icon={faFish} rotation={180} size="2xl" />
                </span>
                </Link>
            </Container>

            <Container className="d-flex
            justify-content-end gap-2">
                <Container className="d-flex justify-content-end align-items-end gap-2">
                    {token

                        ?<Button variant="light" outline="primary" onClick={logOutHandler}>
                            <span>LogOut</span>
                        </Button>
                        :<>
                        <Button variant="outline-primary" onClick={()=>dispatch(setCanvas(showLogin))}>
                            <span>LogIn</span>
                        </Button>
                        <Button variant="outline-primary"  onClick={()=>dispatch(setCanvas(showSignUp))}>
                            <span>SignUp</span>
                        </Button>
                        </>
                    }
                </Container>
                <Button  className="ms-2" onClick={()=>dispatch(setCanvas(showCreateTodoList))}>
                    <FontAwesomeIcon icon={faListCheck} size="2xl" />

                </Button>

            </Container>

        </Container>
    </Navbar>
  )
}
