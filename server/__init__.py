from flask import Flask, jsonify
from flask_cors import CORS
from .extensions import db
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager
import os
from .models.Users import User
from .routes.myTodos import myTodos
from .routes.apiRouter import apiRouter
from .routes.authRouter import authRouter


def create_app():
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DATABASE_URI")
    app.config["JWT_SECRET_KEY"] = os.environ.get("JWT_SECRET_KEY")
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    jwt = JWTManager(app)

    @jwt.user_identity_loader
    def user_identity_lookup(user):
        return user

    @jwt.user_lookup_loader
    def user_lookup_callback(_jwt_header, jwt_data):
        identity = jwt_data["sub"]
        return User.query.filter_by(id=identity).one_or_none()

    @jwt.expired_token_loader
    def my_expired_token_callback(jwt_header, jwt_payload):
        return jsonify(code="dave", err="I can't let you do that"), 403

    CORS(
        app,
        supports_credentials=True,
        origins=["http://localhost", "http://localhost:3000"],
    )

    db.init_app(app)
    migrate = Migrate(app, db)
    with app.app_context():
        db.create_all()

    apiRouter.register_blueprint(authRouter, url_prefix="/auth")
    apiRouter.register_blueprint(myTodos, url_prefix="/todolists")
    app.register_blueprint(apiRouter, url_prefix="/api")
    print([str(p) for p in app.url_map.iter_rules()])

    return app
