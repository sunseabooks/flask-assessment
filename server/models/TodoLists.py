from ..extensions import db
from datetime import datetime
from .Users import User


class TodoList(db.Model):
    __tablename__ = "todolist"

    id = db.Column(db.Integer, primary_key=True)

    title = db.Column(db.String(50), nullable=False)
    created_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    owner_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    todos = db.relationship("Todo", backref="todolist")

    def __repr__(self):
        return "<TodoList %r>" % self.title

    def __init__(self, title, owner_id):
        self.title = title
        self.owner_id = owner_id

    def format_TodoList(self):
        return {"id": self.id, "title": self.title, "created_on": self.created_on}
