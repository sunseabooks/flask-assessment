from ..extensions import db
from datetime import datetime


class User(db.Model):
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)

    username = db.Column(db.String(30), unique=True, nullable=False)
    name = db.Column(db.String(30))
    email = db.Column(db.String(100), unique=True, nullable=False)
    created_on = db.Column(db.DateTime, default=datetime.utcnow)
    password_hash = db.Column(db.String(128))

    todolists = db.relationship("TodoList", backref="owner")

    def __repr__(self):
        return "<Username %r>" % self.password_hash

    def __init__(self, username, name, email, password_hash):
        self.username = username
        self.name = name
        self.email = email
        self.password_hash = password_hash

    def format_User(self):
        return {
            "id": self.id,
            "username": self.username,
            "name": self.name,
            "email": self.email,
        }
