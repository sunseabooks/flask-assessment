from ..extensions import db
from datetime import datetime


class Todo(db.Model):
    __tablename__ = "todo"

    id = db.Column(db.Integer, primary_key=True)

    title = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(100), nullable=False)
    done = db.Column(db.Boolean, nullable=False, default=False)
    due_date = db.Column(db.DateTime, nullable=False)
    created_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    order = db.Column(db.Integer, nullable=False)
    todolist_id = db.Column(db.Integer, db.ForeignKey("todolist.id"))

    def __repr__(self):
        return "<Todo %r>" % self.description

    def __init__(self, title, description, todolist_id, due_date, done, order):
        self.title = title
        self.description = description
        self.todolist_id = todolist_id
        self.due_date = due_date
        self.done = done
        self.order = order

    def format_Todo(self):
        return {
            "id": self.id,
            "title": self.title,
            "description": self.description,
            "done": self.done,
            "order": self.order,
            "created_on": self.created_on,
            "todolist_id": self.todolist_id,
            "due_date": self.due_date,
        }
