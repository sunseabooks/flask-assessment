from ..models.Todos import Todo
from ..extensions import db


class TodosQueries:
    def create(self, content):
        try:
            todo = Todo(**content)
            db.session.add(todo)
            db.session.commit()
            return todo.format_Todo()

        except Exception as e:
            return {"error": f"{e}"}

    def get_all(self, todolist_id):
        try:
            todos = [
                todo.format_Todo()
                for todo in Todo.query.filter_by(todolist_id=todolist_id)
                .order_by(Todo.order.asc())
                .all()
            ]
            return {"todos": todos}

        except Exception as e:
            return {"error": f"{e}"}

    def get_one(self, todo_id):
        try:
            todo = Todo.query.filter_by(id=todo_id).one
            return {"todo": todo.format_Todo()}
        except Exception as e:
            return {"error": f"{e}"}

    def update(self, todo_id, content):
        try:
            todo = Todo.query.filter_by(id=todo_id).first()
            if not todo:
                return {"error": "Todo not found"}

            todo.title = content.get("title", todo.title)
            todo.description = content.get("description", todo.description)
            todo.todolist_id = content.get("todolist_id", todo.todolist_id)
            todo.due_date = content.get("due_date", todo.due_date)
            todo.done = content.get("done", todo.done)
            todo.order = content.get("order", todo.order)

            db.session.commit()
            new_todo = todo.format_Todo()

            return {"todo": new_todo}

        except Exception as e:
            return {"error": f"{e}"}

    def delete(self, todo_id):
        try:
            todo = Todo.query.filter_by(id=todo_id).one()
            db.session.delete(todo)
            db.session.commit()
            return {"deleted": f"{id}"}

        except Exception as e:
            return {"error": f"{e}"}
