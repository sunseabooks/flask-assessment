from ..models.TodoLists import TodoList
from ..extensions import db


class TodoListsQueries:
    def create(self, title, user):
        try:
            todoList = TodoList(title=title, owner_id=user.id)
            db.session.add(todoList)
            db.session.commit()
            return todoList.format_TodoList()

        except Exception as e:
            return {"error": f"{e}"}

    def get_all(self, user):
        try:
            todoLists_list = [
                todolist.format_TodoList()
                for todolist in TodoList.query.filter_by(owner_id=user.id).all()
            ]
            return {"todolists": todoLists_list}

        except Exception as e:
            return {"error": f"{e}"}

    def get_one(self, todolist_id):
        try:
            todolist = TodoList.query.filter_by(id=todolist_id).one()
            return todolist.format_TodoList()

        except Exception as e:
            return {"error": f"{e}"}

    def update(self, todolist_id, content):
        try:
            todolistQuery = TodoList.query.filter_by(id=todolist_id)
            todolistQuery.update(content)
            db.session.commit()
            newTodolist = todolistQuery.one().format_TodoList()
            return {"todolist": newTodolist}

        except Exception as e:
            return {"error": f"{e}"}

    def delete(self, todolist_id):
        try:
            todolist = TodoList.query.filter_by(id=todolist_id).one()
            db.session.delete(todolist)
            db.session.commit()
            return {"deleted": f"{todolist_id}"}

        except Exception as e:
            return {"error": f"{e}"}
