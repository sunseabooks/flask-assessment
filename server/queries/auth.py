from ..models.Users import User
from ..extensions import db
from werkzeug.security import check_password_hash, generate_password_hash
from flask_jwt_extended import (
    create_access_token,
    get_jwt_identity,
    create_refresh_token,
)


class AuthQueries:
    def verify_username(self, username):
        try:
            user = User.query.filter_by(username=username).one()
            return [True, user]
        except Exception as e:
            return [False, f"{e}"]

    def verify_hash(self, hash, password):
        return check_password_hash(hash, password)

    def validate_refresh(self):
        identity = get_jwt_identity()
        access_token = create_access_token(identity=identity)
        return access_token

    def validate_login(self, username, password):
        userStatus, user = self.verify_username(username)
        hashStatus = (
            self.verify_hash(user.password_hash, password) if userStatus else False
        )

        if not userStatus or not hashStatus:
            return {"message": "Wrong username or password"}

        return {
            "access_token": create_access_token(identity=user.id),
            "refresh_token": create_refresh_token(identity=user.id),
            "user": user.format_User(),
        }

    def create_user(self, content):
        usernameTaken, _ = self.verify_username(content.get("username"))

        if usernameTaken:
            return [False, {"message": "username taken"}]

        password = content.pop("password")
        content["password_hash"] = generate_password_hash(password=password)
        try:
            user = User(**content)
            db.session.add(user)
            db.session.commit()
            return [True, user.format_User()]

        except Exception as e:
            return {"Error": f"EEEEEEEEEEEError {e}"}
