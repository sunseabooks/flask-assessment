#!/bin/bash

sleep 5

echo "Initializing db"
flask db init

echo "creating migration"
flask db migrate -m "Initial migration"

echo "Upgrading database"
flask db upgrade

echo "running server"
exec "$@"
