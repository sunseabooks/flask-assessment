from flask import Blueprint, request, jsonify
from ..queries.auth import AuthQueries
from flask_jwt_extended import jwt_required


authRouter = Blueprint("authRouter", __name__)
query = AuthQueries()


@authRouter.route("/token", methods=["POST"])
def login():
    data = request.get_json()
    auth = query.validate_login(
        username=data.get("username"), password=data.get("password")
    )
    return jsonify(auth)


@authRouter.route("/token/refresh", methods=["GET"])
@jwt_required(refresh=True)
def refresh_token():
    auth = query.validate_refresh()
    return jsonify(auth)


@authRouter.route("/signup", methods=["POST"])
def signup():
    data = request.get_json()
    content = {
        "username": data.get("username"),
        "name": data.get("name"),
        "password": data.get("password"),
        "email": data.get("email"),
    }

    hasCreatedUser, res = query.create_user(content)
    if hasCreatedUser:
        access_token = query.validate_login(
            username=res.get("username"), password=data.get("password")
        )
        return jsonify(access_token)
    else:
        return jsonify(res.get("message"))
