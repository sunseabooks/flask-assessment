from flask import Blueprint, request
from ..queries.TodoLists import TodoListsQueries
from ..queries.Todos import TodosQueries
from flask_jwt_extended import jwt_required, current_user
from datetime import datetime


myTodos = Blueprint("myTodos", __name__)

TodoListsQuery = TodoListsQueries()
TodosQuery = TodosQueries()


# these are routes for todolists
@myTodos.route("", methods=["POST", "GET"])
@jwt_required()
def todolists_listsRouter():
    if request.method == "POST":
        return TodoListsQuery.create(title=request.json["title"], user=current_user)

    if request.method == "GET":
        return TodoListsQuery.get_all(user=current_user)


@myTodos.route("/<int:todolist_id>", methods=["GET", "PUT", "DELETE"])
@jwt_required()
def todolists_detailsRouter(todolist_id):
    if request.method == "GET":
        return TodoListsQuery.get_one(todolist_id)

    if request.method == "PUT":
        content = {"title": request.json["title"], "created_on": datetime.utcnow()}
        return TodoListsQuery.update(todolist_id=todolist_id, content=content)

    if request.method == "DELETE":
        return TodoListsQuery.delete(todolist_id)


# these are routes for todos
@myTodos.route("/<int:todolist_id>/todos", methods=["POST", "GET"])
@jwt_required()
def todos_listsRouter(todolist_id):
    if request.method == "POST":
        content = {
            "title": request.json["title"],
            "description": request.json["description"],
            "due_date": request.json["due_date"],
            "done": request.json["done"],
            "todolist_id": request.json["todolist_id"],
            "order": request.json["order"],
        }
        return TodosQuery.create(content)

    if request.method == "GET":
        return TodosQuery.get_all(todolist_id)


@myTodos.route(
    "/<int:todolist_id>/todos/<int:todo_id>", methods=["GET", "PUT", "DELETE"]
)
@jwt_required()
def todos_detailsRouter(todolist_id, todo_id):
    if request.method == "GET":
        return TodosQuery.get_one(todo_id)

    if request.method == "PUT":
        content = {
            "title": request.json["title"],
            "description": request.json["description"],
            "done": request.json["done"],
            "due_date": request.json["due_date"],
            "created_on": datetime.utcnow(),
            "order": request.json["order"],
            "todolist_id": todolist_id,
        }
        return TodosQuery.update(todo_id, content)

    if request.method == "DELETE":
        return TodosQuery.delete(todo_id)
