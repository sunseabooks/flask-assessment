#!/bin/bash

sleep 5
echo "init db"
flask db init

echo "creating migration"
flask db migrate -m "init migration"

echo "upgradeing db"
flask db upgrade

echo "running server"
exec "$@"
